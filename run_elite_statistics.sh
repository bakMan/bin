#! /bin/bash
source runs.conf

if [[ ! $# -eq 6 ]]; then
	echo "Bad number of parameters, expecting min max step pg crossover_rate num_runs." 
	exit 1
fi

min=$1
max=$2
step=$3
pg=$4
crossover_rate=$5
num_runs=$6

#cleanup
rm -f 3dplot_elite.txt

echo "Performing experiment for P*G=$pg, $CL_FILE, $DESIGN_FILE, $crossover_rate crossover,$num_runs runs"

#set variables for gnuplot
echo "pg = $pg" > plot/imports/3dplot_var_elite.plt

num=0
for num_children in `seq $min $step $max`; do
	((num_gen=$pg / $num_children))	

	for elite_rate in `LANG=en_US seq 0 0.1 0.8`; do
		#run multiple times and calculate average best fitness
		sum=0
		elite_rate=$(echo $elite_rate | tr ',' '.')

		for (( i = 0; i < $num_runs; i++ )); do
			#get best fitness value
			best=$(python3 src/genetic.py -n $num_gen -c $num_children -e $elite_rate -t $DELAY $CL_FILE $DESIGN_FILE | grep 'Best fitness' | cut -d ':' -f 2 | xargs)
			((sum=$sum + $best))

		done

		if [[ $((num%4)) -eq 0 && ! $num -eq 0 ]]; then
			echo "" >> output/3dplot_elite.txt
		fi

		#calculate average
		avg=$(bc <<< "scale=2;$sum/$num_runs")
		
		#output
		echo "$num_children $elite_rate $avg" >> output/3dplot_elite.txt
		((num=num + 1))
	
	done

done