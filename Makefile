include runs.conf 

GRAPH_DIR=graph

run: prebuilds
	./run.sh
	cd plot && gnuplot plot_graphs.plt

#####################################
#prebuilds: $(BIN_DIR) $(OBJ_DIR) $(DEP_DIR)
prebuilds:
	mkdir -p $(GRAPH_DIR) doc/ doc/import output/ plot/ plot/import 



############

# ship:
# 	make -C ../doc
# 	cp ../doc/proj4.pdf dokumentace.pdf
# 	zip -r xbenes20 src/ Makefile

slides: replot
	cd doc && make

clean-output: 
	rm -f output/*

clean: clean-output
	rm -f $(GRAPH_DIR)/*
	rm -f xsmata01.zip xsmata01.tar.gz
	cd doc && make clean

mutation_stats:
	./run_mutation_statistics.sh 10 100 10 1000 $(N_RUNS)
	
elite_stats:
	./run_elite_statistics.sh 10 100 10 1000 0.2 $(N_RUNS)

mutation_box:
	./run_box.sh 1000 10 0.0 0.1 0.9 $(N_RUNS) mutation 
	
elitism_box:
	./run_box.sh 1000 10 0.0 0.1 0.8 $(N_RUNS) elitism 
	
t_test:
	./stat_tests.py output/mutation_box.txt t > output/t_test_mutation.txt
	./stat_tests.py output/elitism_box.txt t > output/t_test_elite.txt
	cat output/t_test_mutation.txt | awk -f ttest_to_latex.awk > doc/import/t_mutation.tex
	cat output/t_test_elite.txt | awk -f ttest_to_latex.awk > doc/import/t_elitism.tex

replot:
	cd plot && make

experiment: prebuilds clean-output mutation_stats elite_stats mutation_box elitism_box t_test
	make replot
	
test: 
	./run_test.sh
	
ship: clean
	cp doc/slides.pdf ./
	zip -r xsmata01.zip Makefile src/* plot/* doc/* input/* *.sh *.awk *.py slides.pdf README.txt runs.conf
	rm slides.pdf
	#tar cvzf xsmata01.tar.gz Makefile src/ Makefile run.sh run_test.sh

test_merlin: clean ship
	./test_merlin.sh

everything: clean experiment replot slides

.PHONY: clean ship
