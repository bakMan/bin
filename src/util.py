def output_statistics(filename,stats):
	with open(filename,"w") as f:
		for (top,avg) in stats:
			f.write(str(top) + " " + str(avg) + "\n");
