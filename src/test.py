import evolution
import random
import math
import util
import genetic

def fitness_ackley(chromosome):
	firstSum = 0.0;
	secondSum = 0.0;
	for c in chromosome:
		firstSum += c ** 2
		secondSum += math.cos(2.0*math.pi*c)
		n = float(len(chromosome))
	return 14 - (-20.0*math.exp(-0.2*math.sqrt(firstSum/n)) - math.exp(secondSum/n) + 20 + math.e)


def mkMutator(mu,sigma):
	def mutate(chromosome):
		new_chrom = []
		for c in chromosome:
			new_chrom.append( c + random.normalvariate(mu, sigma));
		return new_chrom;
	return mutate;

def mkRandom(max_x,max_y,origin_x = 0,origin_y=0):
	def rand():
		return [origin_x + random.random()*max_x*2 - max_x,origin_y + random.random()*max_y * 2 - max_y];
	return rand;

def main():
	print("Testing evolution on ackley function")
	rand_f = mkRandom(5,5);
	mutator = mkMutator(0,0.1);

	#list for average/top fitness collection
	statistics = [];
	(f,val) = evolution.evolve(fitness_ackley,rand_f,mutator,num_gener=60,num_per_gen=400,
							stat_col= lambda x, y: statistics.append((x,y)));
	

	print("Found value ({0:.4f}, ".format(val[0]) + "{0:.4f})".format(val[1]) + " with fitness " + "{0:.4f}".format(f));
	print("RESULT: ", end="")
	
	if abs(val[0] - 0.0) < 0.001 and abs(val[1] - 0.0) < 0.001:
		print("PASS")
	else:
		print("FAIL")

	util.output_statistics("output/out.txt",statistics);

if __name__ == '__main__':
	main()
	