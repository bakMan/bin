import random

def evolve(fitness_func,random_func,mutation_func,num_gener=100,
			num_per_gen=100,CROSS_THRESH = 0.6,RATIO_ELITE = 0.1,stat_col=None):
	#constants
	#percent chromosomes from old generation

	NUM_CROSS = 0;
	NUM_MUT = 0;

	#Our ultimate winner !
	fittest = (0,None);

	#initialize pool with random chromosomes
	pool = [ random_func() for x in range(num_per_gen)];
	pool.sort(key= lambda x: fitness_func(x),reverse=True);

	#helper array for new generation
	new_pool = [None for x in range(len(pool))];

	#until max number of generations is reached
	for t in range(num_gener):

		#Calculate number of elite chromosomes
		num_elites = round(num_per_gen * RATIO_ELITE)
		num_new    = num_per_gen - num_elites;

		#ensure number of new genes in pool is even
		#num_elites = num_elites + num_new % 2

		#Copy elites
		for i in range(num_elites):
			new_pool[i] = pool[i];

		#generate others using crossower and mutation
		for i in range(num_elites,len(pool)):
			
			#select two chromosomes using 2-tournament
			a = tournament(2,fitness_func,pool);
			b = tournament(2,fitness_func,pool);
			
			#decide crossover vs mutation
			rnd = random.uniform(0.0,1.0);
			
			#apply crossover or mutation
			c = None
			d = None

			if rnd <= CROSS_THRESH:
				(c,d) = crossover(a,b);
				NUM_CROSS = NUM_CROSS +1;
			else:
				c = mutation_func(a);
				d = mutation_func(b);
				NUM_MUT = NUM_MUT + 1;

			new_pool[i] = c;

			#in case pool has odd size, this would be out of range
			if i != len(pool) - 1:
				new_pool[i+1] = d;

		#Switch variables
		pool,new_pool = new_pool,pool;
	
		#sort pool for elity choosing
		pool.sort(key= lambda x: fitness_func(x),reverse=True);
		top = fitness_func(pool[0]);

		if fittest[0] < top:
			fittest = (top,pool[0]);

		#in case of stat sending calculate additional statistics
		if stat_col != None:
			avg = sum([fitness_func(x) for x in pool]) / float(len(pool));
			stat_col(top,avg);



		#TODO: Statistics	
	#debug
	print("Crossovers: " + str(NUM_CROSS));
	print("Mutations: " + str(NUM_MUT));

	return fittest;

#one point crossower - independend of chromosome type
def crossover(a,b):
	c = a.copy();
	d = b.copy();

	#crossover point
	pt =random.randrange(len(a)); 

	#switch on crossover point and above
	for i in range(pt,len(c)):
		tmp = c[i];
		c[i] = d[i];
		d[i] = tmp;
	return (c,d);

#k-tournament selection
def tournament(k,fitness_func,pool):
	candidates = []
	for i in range(k):
		ch = random.choice(pool);
		candidates.append((fitness_func(ch),ch));
	
	(f,e) = max(candidates,key= lambda x: x[0]);
	return e;