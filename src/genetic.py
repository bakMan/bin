#! /usr/bin/python3
import random
import json
from optparse import OptionParser
import evolution
import util
import sys

#Example problem instance, writable to file by -m option
G_CL = {
	"MPY" : [
		(4500,21),
		(4000,23),
		(3500,25),
		(2900,29),
		(2700,33),
		(2600,36),
		(2400,44),
		(2300,58)
	],
	"ADD" : [
		(500,3),
		(400,6),
		(250,10),
		(200,14),
		(100,20),
		(50,26)
	],
	"SUB" : [
		(500,3),
		(400,6),
		(250,10),
		(200,14),
		(100,20),
		(50,26)
	],
	"SEP" : [],
}


G_DESIGN = ["ADD","ADD","SEP",
			"ADD","SEP",
			"ADD","SEP",
			"ADD","SEP",
		    "MPY","MPY", "SEP", 
		    "ADD", "ADD","SEP",
		    "ADD" ,"ADD", "SEP",
		    "ADD", "MPY","MPY","SEP",
		    "ADD", "ADD","SEP",
		    "ADD", "ADD","SEP",
		    "ADD", "ADD","SEP",
		    "ADD", "MPY","ADD","MPY","SEP",
		    "MPY", "ADD","MPY","ADD","SEP",
		    "ADD", "ADD","SEP",
		    "ADD", "ADD","SEP",
		    "ADD", "ADD","SEP"
		   ];



#Rebuilds CL datastructure with name assigned to every component
def generateNamed(cl):
	#init new struct
	new_cl = {};
	for	key in cl.keys():
		#init value in new structure
		items = cl[key];
		new_cl[key] = [];
		numberer = 1
		for val in reversed(items):
			(a,b) = val #unpack
			#make triplet "NAME + NUM" and old information
			new_cl[key].append((key + str(numberer),a,b) );
			numberer = numberer +1;
	return new_cl

#based on the design and cl, function generates random component selection
def mkrandFunc(cl,recipe):
	def randomChromosome():
		chrom = [];

		for val in recipe:
			
			#get number of avaliable components of that type in CL
			num_entries = len(cl[val]);

			#for sep do nothing
			if num_entries != 0:
				rnd = random.choice(cl[val]);
				chrom.append(rnd);
		
		return chrom;

	return randomChromosome;

def mkFitness(recipe,req_latency = 200):
	#precalculate ends of csteps
	sep_idxs = [];
	num = 0;

	for val in recipe:
		if val == "SEP":
			sep_idxs.append(num-1);
		else:
			num = num + 1;
	#print(sep_idxs)

	def fitness(chromosome,verbose = False):
		sum_t = 0;
		max_t = 0; #no component will have delay of 0
		_sep_idx = 0; 

		for i in range(len(chromosome)):

			#now interested only in time
			(_,_,t) = chromosome[i];
			
			if t > max_t:
					max_t = t;

			if i == sep_idxs[_sep_idx]:
				#print(max_t);
				sum_t = sum_t + max_t;
				max_t = 0; 
				_sep_idx = _sep_idx + 1;

		#print("SUM: " + str(sum_t))

		#TODO-CHECK second part - space on chip
		sum_space = sum (map(lambda x : x[1], chromosome));

		if verbose:
			return (sum_t,sum_space);

		if sum_t > req_latency:
			return 1;
		else:
			#return 40000 - sum_space - abs(sum_t - req_latency)*5;
			return 1000000 - sum_space - abs(sum_t - req_latency)*5;
						

	return fitness;


#Mutation has to have knowledge about chromosome representation
#therefore it is passed to evolve from outside
#this is unlike crossover, which is completely repr-free
def mkMutate(cl,design):
	#separators are not in chromosome
	# must be removed for lookup
	design = [x for x in design if x != "SEP"];

	def mutate(a):		
		#choose mutation point
		pt =random.randrange(len(a));
		#what type of component it is ?
		component_type = design[pt];
		#choos random component of that type
		b = a.copy();
		b[pt] = random.choice(cl[component_type]);

		return b;

	return mutate;

def dumpChromosome(chrom,design):
	idx = 0;

	for val in design:
		if val == "SEP":
			print(""); #newline
		else:
			(name,_,_) = chrom[idx];
			print(name + " ",end="");
			idx = idx + 1;


def main():


	#commandline options
	parser = OptionParser(usage="genetic.py [options] cl_file design_file");
	
	parser.add_option("-g","--graph-output",dest="graph_file",
		help="Output evolution process statistics for graph plotting.");
	
	parser.add_option("-c","--children-in-gen",type="int",dest="children", default=100,
		help="Number of children per generation, defaults to 100.");
	
	parser.add_option("-n","--number-of-gen",type="int",dest="generations", default=100,
		help="Number of generations, defaults to 100.");
	
	parser.add_option("-p","--probability-crossover",type="float",dest="crossover", default=0.6,
		help="Probability of crossover, 1 - mutation probability, defaults to 0.6.");
	
	parser.add_option("-e","--elite-rate",type="float",dest="elite", default=0.1,
		help="Rate of elitism,percentual part of species that are preserved between generations, defaults to 0.1");
	
	parser.add_option("-m","--make-example",action="store_true",dest="example",
		help="Dump example JSON problem description into example_cl.json and example_design.json and exit program.");
	
	parser.add_option("-t","--target-delay",action="store",type="int",dest="delay",
		help="REQUIRED, target delay of evolutionary design.");

	(options,args) = parser.parse_args();
	
	#on parameter make example, create example json files for problem instance and quit
	if options.example:
		with open("example_cl.json","w") as f:
			f.write(json.dumps(G_CL));
		with open("example_design.json","w") as f:
			f.write(json.dumps(G_DESIGN));

		print("Written example_cl.json and example_design.json")
		sys.exit(0);

	#load problem instance
	CL=None
	DESIGN=None

	try:
		if len(args) != 2:
			raise Exception("Program requires two positional arguments - cl library and design file")

		with open(args[0]) as f:
			CL = json.loads(f.read());

		with open(args[1]) as f:
			DESIGN = json.loads(f.read());

	except Exception as e:
		sys.stderr.write(str(e) + "\n");
		sys.exit(1);

	#load timing
	DELAY=None
	if options.delay:
		DELAY = options.delay;
	else:
		sys.stderr.write("Error -t parameter is required\n");
		sys.exit(1);

	#generate names for components
	CL = generateNamed(CL);

	#create evolution algorithm functions
	fitness = mkFitness(DESIGN,DELAY);
	rand_func = mkrandFunc(CL,DESIGN);
	mutate = mkMutate(CL,DESIGN);

	#list for average/top fitness collection
	statistics = [];
	
	if options.graph_file:
		(f,chrom) = evolution.evolve(fitness,rand_func,mutate,options.generations,options.children,options.crossover,options.elite,
						stat_col= lambda x, y: statistics.append((x,y)) );

	else:
		(f,chrom) = evolution.evolve(fitness,rand_func,mutate,options.generations,options.children,options.crossover,options.elite);

	
	print("Best fitness: " + str(f));
	dumpChromosome(chrom,DESIGN);

	#calculate timing and space separately
	(timing,space) = fitness(chrom,verbose=True);
	print("Desing timing: " + str(timing) + " (target timing " + str(DELAY) + ")");
	print("Desing space: " + str(space));

	if  options.graph_file:
		util.output_statistics(options.graph_file,statistics);

if __name__ == '__main__':
	main()