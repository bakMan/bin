#! /bin/bash
source runs.conf

if [[ ! $# -eq 5 ]]; then
	echo "Bad number of parameters, expecting min max step pg num_runs." 
	exit 1
fi

min=$1
max=$2
step=$3
pg=$4
num_runs=$5

#cleanup

echo "Performing experiment for P*G=$pg, $CL_FILE, $DESIGN_FILE, $num_runs runs"


#set variables for gnuplot
echo "pg = $pg" > plot/imports/3dplot_var_mut.plt


num=0
for num_children in `seq $min $step $max`; do
	((num_gen=$pg / $num_children))	

	for cross_rate in `LANG=en_US seq 0 0.2 1.0`; do
		#run multiple times and calculate average best fitness
		sum=0
		cross_rate=$(echo $cross_rate | tr ',' '.')

		for (( i = 0; i < $num_runs; i++ )); do
			#get best fitness value
			best=$(python3 src/genetic.py -n $num_gen -c $num_children -p $cross_rate -t $DELAY $CL_FILE $DESIGN_FILE | grep 'Best fitness' | cut -d ':' -f 2 | xargs)
			((sum=$sum + $best))

		done

		if [[ $((num%4)) -eq 0 && ! $num -eq 0 ]]; then
			echo "" >> output/3dplot.txt
		fi

		#calculate average
		avg=$(bc <<< "scale=2;$sum/$num_runs")
		
		#output
		echo "$num_children $cross_rate $avg" >> output/3dplot.txt
		((num=num + 1))
	
	done

done