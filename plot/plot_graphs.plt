set output "../graph/fitness.pdf"
set terminal pdf
set key right bottom
set title "Fitness function developement during evolution"

set xlabel "Generation"
set ylabel "Fitness"

plot "../output/out.txt" using 1 title "Top speciman" with lines, "../output/out.txt" using 2 title "Generation average" with lines
