#! /usr/bin/python

#todo mam testy spravne ?

import sys
import scipy.stats

class InvalidSyntax(Exception):
	pass;

#program needs one parameter
if len(sys.argv) != 3:
	sys.stderr.write("Program expects two parameters - box plot data file and test type 't' or 'f'\n");
	sys.exit(1)

if not sys.argv[2] in ["t","f"]:
	sys.stderr.write("Test type must be either 'f' or 't'.\n")
	sys.exit(1);

#File processing
filename=sys.argv[1];
TEST=sys.argv[2];

EXIT_STATUS=0;

try:
	#take second column unique values, for reference
	tags=set()
	#dictionary of lists for data
	data=dict()

	with open(filename) as f:
		#line by line cut columns and add to dict and set
		lnum=0;
		for line in f:
			lnum +=1;

			#Two column file required
			columns = line.split();
			if len(columns) != 2:
				raise InvalidSyntax("File must be in 2 column format, on line " + str(lnum) );

			tags.add(columns[1]);

			#if there are no records for given tag - add empty
			if not columns[1] in data:
				data[columns[1]] = [];

			data[columns[1]].append(float(columns[0]));

	#statistical testing
	#t test
	#header
	tags = sorted(list(tags))

	if TEST=='t':
		for t in tags:
			sys.stdout.write(" " + t + " ");
		print("");
		
		for tag1 in tags:
			for tag2 in tags:
				t_val = scipy.stats.ttest_ind(data[tag1], data[tag2], axis=0, equal_var=False);
				sys.stdout.write("{:.2f}".format(t_val[1]) + " ");

			print("\n");
	else:
		f_val = scipy.stats.f_oneway(*[data[k] for k in tags]);
		print(f_val[1]);

except Exception as e:
	sys.stderr.write("Error processing file '" + filename + "'\n " + str(e) + "\n");
	EXIT_STATUS=1;
finally:	
	sys.exit(EXIT_STATUS)