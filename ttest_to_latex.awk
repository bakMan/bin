NF {
	#not for first line
	if (NR != 1)
	{

		#diagonal matrix, first blanks
		for(i = 0;i < num;i++)
		{
			printf " &";
		}

		#now data
		for(i = num;i <= NF;i++)
		{
			if ($i <0.05)
			{
				printf "\\cellcolor{red!25}"
			}
			else
			{
				printf "\\cellcolor{green!25}"
			}

			printf " %.2f &",$i;
		}

		#add corresponding header to side
		printf " %s &", HEADER_ARR[num - 1];

		print "\\\\";
		num ++;
	}
	else
	{
		#print header
		printf "\\begin{tabular}{";

		# <= NF -> one extra column for side header
		for(i = 0;i <= NF + 1;i++)
		{
			printf " c ";
		}

		print "}"

		#print header of data
		#header is shifted by one -> field 0,0 is identity
		printf " &";
		for(i = 1;i <= NF;i++)
		{
			HEADER_ARR[i - 1] = $i
			printf "%s &",$i;
		}

		print "\\\\";

		num = 1;
	}
}

END { print "\\end{tabular}"}