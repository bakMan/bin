#! /bin/bash
source runs.conf


if [[ ! $# -eq 7 ]]; then
	echo "Bad number of parameters, expecting num_gens num_children min step max num_runs type." 
	exit 1
fi

num_gens=$1
num_children=$2
min=$3
step=$4
max=$5
num_runs=$6
type=$7

if [[ $type != "elitism" && $type != "mutation" ]]; then
	echo "Last parameter should be either elitism of mutation"
	exit 1
fi

echo "Performing experiment for P*G=$pg, $CL_FILE, $DESIGN_FILE,$num_runs runs, $type"

#data for gnuplot
echo -e "gen = $num_gens \n children=$num_children \n runs=$num_runs" > plot/imports/box_var.plt


for (( i = 0; i < $num_runs; i++ )); do
	
	#cross rate is the opposite of mutation rate
	for cross in `seq $max -$step $min`; do
		cross=$(echo $cross | tr ',' '.')
		
		#if mutation is set, parameter value is set for mutation
		#otherwise for elitism
		if [[ $type == "mutation" ]]; then
			best=$(python3 src/genetic.py -n $num_gens -c $num_children -p $cross -t $DELAY $CL_FILE $DESIGN_FILE | grep 'Best fitness' | cut -d ':' -f 2 | xargs)
			mut=$(echo "1.0 $cross" |  awk '{printf "%.1f", $1 - $2}')
			echo "$best $mut" >> output/mutation_box.txt
		else
			best=$(python3 src/genetic.py -n $num_gens -c $num_children -p $CROSSOVER -e $cross -t $DELAY $CL_FILE $DESIGN_FILE | grep 'Best fitness' | cut -d ':' -f 2 | xargs)
			echo "$best $cross" >> output/elitism_box.txt
		fi

	done

#	echo "" >> mutation_box.txt
done

#FIX - elitism has to be sorted otherwise x is flipped
cat output/elitism_box.txt | sort -k2 > output/tmp
mv output/tmp output/elitism_box.txt